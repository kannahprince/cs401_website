<?php include_once("Header.php"); ?>
    <html>

    <head>
        <title>Cty-View</title>
        <!-- See header.php for CSS info -->
    </head>

    <body>
    <main>
        <div class="container">
            <div class="row center">
                <img id="front_logo" class="cols s8 offset-s2 offset-m3 m6 responsive-img" src="images/cty_logo.png">
            </div>
            <!-- LANDING PAGE SEARCH bar -->
            <div class="row center">
                <div row="">
                    <form accept-charset="UTF-8" action="SearchHandler.php" method="get">
                        <div style="display:none">
                            <input name="utf8" type="hidden" value="✓">
                        </div>
                        <div class="input-field gray lighten-5 col s12 m12 18 offset-12">
                            <input class="validate front-search ui-autocomplete-input" id="search_placeholder"
                                   minlength="3" name="search" placeholder="Enter a city, state, or zip-code"
                                   type="text">
                        </div>
                        <!-- SEARCH BUTTON -->
                        <div class="row">
                            <div class="col offset-s9">
                                <input class="deep-purple btn offset-s3" value="search" type="submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <div class="hiddendiv common"></div>
    </body>
    </html>
<?php include_once("Footer.php"); ?>