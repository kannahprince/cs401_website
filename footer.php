<!DOCTYPE html>
<html>

<head>
    <!-- See header.php for CSS info -->
</head>

<body>
<footer class="page-footer white">
    <div class="footer-copyright grey lighten-5">
        <div class="container ">
            <div class="black-text darken">&copy 2017 Cty-View. All Rights Reserved.</div>
        </div>
    </div>
</footer>
</body>

</html>