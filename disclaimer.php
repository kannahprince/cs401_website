<?php include_once("Header.php"); ?>
    <html>

    <head>
        <title>Disclaimer - just to be safe</title>
        <!-- See header.php for CSS info -->
    </head>

    <body>
    <main>
        <div class="container">
            <div class="blog-post">
                <h3 class="blog-post-title">Disclaimer</h3>
                <p>This website is NOT a official US government website. It is not sponsored or supported by the US
                    government. I simply use their <a href="https://github.com/uscensusbureau/citysdk" target="_blank">
                        APIs</a> to access the necessary data. This website was built as a <a
                            href="http://cs.boisestate.edu/~ckenning/401/teaching.php" target="_blank">class project</a>.
                </p>
                <p>If you have feedback or questions you can get in touch with me <a
                            href="mailto:princekannah@u.boisestate.edu">here</a>.</p>
            </div>
        </div>
    </main>
    </body>

    </html>
<?php include_once("Footer.php"); ?>