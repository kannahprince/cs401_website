<?php //include_once("Header.php"); ?>
    <html>

    <head>
        <title>Search</title>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
        <link href="css/MapFeatures.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="handlers_scripts/MapFunctions.js" type="application/javascript"></script>
        <!-- See Header.php for additional CSS info -->
    </head>

    <body>
    <main>
        <div>
            <div id="controls" class="control_box">
                <div>
                    <select id="census-variable">
                        <option value="https://storage.googleapis.com/mapsdevsite/json/DP02_0066PE">Percent of
                            population
                            over 25 that completed high
                            school
                        </option>
                        <option value="https://storage.googleapis.com/mapsdevsite/json/DP05_0017E">Median age</option>
                        <option value="https://storage.googleapis.com/mapsdevsite/json/DP05_0001E" selected>Total population
                        </option>
                        <option value="https://storage.googleapis.com/mapsdevsite/json/DP02_0016E">Avg family size
                        </option>
                        <option value="https://storage.googleapis.com/mapsdevsite/json/DP03_0088E">Per-capita income
                        </option>
                    </select>
                </div>
                <div id="legend">
                    <div id="census-min">min</div>
                    <div class="color-key"><span id="data-caret">&#x25c6;</span></div>
                    <div id="census-max">max</div>
                </div>
            </div>
            <div id="data-box" class="control_box">
                <label id="data-label" for="data-value"></label>
                <span id="data-value"></span>
            </div>
            <div id="map"></div>
        </div>
    </main>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=API_KEY&callback=initMap">
    </script>

    </body>
    </html>
<?php //include("Footer.php"); ?>