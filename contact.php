<?php include("Header.php") ?>
    <html>

    <head>
        <title>Contact - let's get in touch</title>
        <!-- See header.php for CSS info -->
    </head>

    <body>
    <main>
        <div class="container">
            <h3>Let's get in touch</h3>
            <p>Feel free to send me a message to let me know what you think! This is an early stage project, so your
                feedback and support is valued and appreciated. With that being said, I'm kinda busy, so it may be some
                time before I get back to you.
            </p>
            <form accept-charset="UTF-8" action="classes/send_message.php" class="new_message" id="new_message"
                  method="post">
                <div style="display:none">
                    <input name="utf8" type="hidden" value="✓">
                </div>
                <div class="form-group">
                    <label for="message_name" class="">Name:</label>
                    <br>
                    <input id="message_name" name="name" required="required" type="text">
                </div>
                <div class="form-group">
                    <label for="message_email" class="">Email:</label>
                    <br>
                    <input id="message_email" name="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required="required"
                           type="text">
                </div>
                <div class="form-group">
                    <label for="message_subject">Subject:</label>
                    <br>
                    <input id="message_subject" name="subject" required="required" type="text">
                </div>
                <div class="form-group">
                    <label for="message_body" class="">Message:</label>
                    <br>
                    <textarea class="materialize-textarea" id="message_body" name="body" required="required"
                              style="height: 22px;"></textarea>
                </div>

                <div class="form-group">
                    <input class="deep-purple btn" name="commit" type="submit" value="Send Message">
                </div>
            </form>
        </div>
    </main>

    </body>

    </html>
<?php include("Footer.php") ?>