<?php include("Header.php"); ?>
    <html>

    <head>
        <title>Sign-up</title>
        <!-- See header.php for CSS info -->
    </head>

    <body>
    <main>
        <div class="container" align="center" style="margin-top:150px;">
            <h3>Sign-up for Cty-View</h3>
            <br>
            <form accept-charset="UTF-8" action="handlers_scripts/SignupHandler.php" class="new_message"
                  id="new_message"
                  method="post">
                <div style="display:none">
                    <input name="utf8" type="hidden" value="✓">
                </div>
                <div class="form-group">
                    <label for="first_name" class="">First-name:</label>
                    <br>
                    <input autofocus="autofocus" class="form-control input-block" id="first_name" name="first_name"
                           required="required" type="text">
                </div>
                <br>
                <div class="form-group">
                    <label for="last_name" class="">Last-name:</label>
                    <br>
                    <input id="last_name" name="last_name" required="required" type="text">
                </div>

                <div class="form-group">
                    <label for="message_email" class="">Email:</label>
                    <br>
                    <input id="message_email" name="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required="required"
                           type="text">
                </div>

                <div class="form-group">
                    <label for="password" class="">Password:</label>
                    <br>
                    <input class="form-control form-control input-block" id="password" name="password" type="password"
                           required="required">
                </div>
                <div class="form-group">
                    <input class="deep-purple btn" name="commit" type="submit" value="Sign-up">
                </div>
            </form>
        </div>
    </main>

    </body>

    </html>
<?php include("Footer.php"); ?>