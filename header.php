<!-- Start PHP session -->
<?php session_start(); ?>
<html>

<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" media="screen" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" media="screen" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" media="screen" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" media="screen" rel="stylesheet" type="text/css">
    <link href="css/Materialize.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="css/Application.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="css/Home.css" media="screen" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
</head>

<body>
<!-- Nav menu bar -->
<div class="header">
    <nav class="white black-text" role="navigation">
        <div class="nav-wrapper">
            <a id="logo-container" class="brand-logo" href="index.php">
                <img id="navbar_logo" src="images/cty_logo.png">
            </a>
            <!-- Nav menu options -->
            <ul class="right hide-on-med-and-down">
                <?php if ($_SESSION['AUTH']): ?>

                    <li>
                        <!-- Display user name -->
                        <a class="deep-purple-text"> <?php echo $_SESSION['name'] ?></a>

                        <!-- Show logout when mouse hovers on user name -->
                        <ul class="dropdown-content">
                            <li>
                                <a class="deep-purple-text" href="Logout.php">Log-out</a>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li>
                        <a class="deep-purple-text" href="Login.php">Login</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a class="deep-purple-text" href="About.php">About</a>
                </li>
                <li>
                    <a class="deep-purple-text" href="Disclaimer.php">Disclaimer</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

</body>

</html>