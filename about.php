<?php include("Header.php"); ?>
    <html>

    <head>
        <title>About</title>
        <!-- See header.php for CSS info -->
    </head>

    <body>
    <main>
        <div class="container">

            <div class="blog-post">
                <h3 class="blog-post-title">About Cty-View</h3>
                <h5 class="lead deep-purple-text">Your community...expanded<br></h5>
                <p>Cty View is an interactive, API driven website built on top of <a href="http://www.census.gov/"
                                                                                     target="_blank">US Census</a> data.
                    The goal is to provide a simple, easy to use interface for you to play around with the wealth of
                    data provided by the Census.
                </p>

                <h5>About Me</h5>
                <p>My name is Prince. I'm currently a student at Boise State University studying Computer Science. I
                    work in the High Performance Computing lab under <a
                            href="https://coen.boisestate.edu/catherineolschan/" target="_blank">Dr. Catherine
                        Olschanowsky</a> currently looking into compiler optimization. When I'm not doing homework or
                    working you can find me sinking countless hours into games (<a
                            href="http://www.metacritic.com/game/pc/doom">Doom</a>, <a
                            href="http://www.metacritic.com/game/pc/dishonored-2">Dishonred 2</a>, and <a
                            href="http://www.metacritic.com/game/pc/no-mans-sky">No Man's Sky</a> at the moment), crying
                    from laughter because of <a href="https://reddit.com/r/BlackPeopleTwitter" target="_blank">black
                        people twitter</a> or just hanging out with bae.
                </p>

                <h5>Feedback/Questions</h5>
                <p>Please direct your questions and feebacks <a href="mailto:princekannah@u.boisestate.edu">here</a>.
                </p>

            </div>
        </div>

    </main>
    </body>

    </html>
<?php include("Footer.php"); ?>