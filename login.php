<?php include("Header.php"); ?>
    <html>

    <head>
        <!-- See header.php for CSS info -->
        <title>Sign-in to Cty-View</title>
    </head>

    <body>
    <main>
        <div class="container" align="center" style="margin-top:160px;">
            <h3>Sign into Cty-View</h3>
            <form accept-charset="UTF-8" action="handlers_scripts/LoginHandler.php" class="new_message" id="new_message"
                  method="post"
                  style="margin:auto">
                <div style="display:none">
                    <input name="utf8" type="hidden" value="✓">
                </div>
                <div class="form-group">
                    <label for="login_field">EMAIL ADDRESS</label>
                    <br>
                    <input autofocus="autofocus" class="form-control input-block" id="login_field" name="email"
                           type="text" required="required">
                </div>
                <div class="form-group">
                    <label for="password">PASSWORD</label>
                    <br>
                    <input class="form-control form-control input-block" id="password" name="password" type="password"
                           required="required">
                </div>
                <div class="form-group">
                    <input class="deep-purple btn" name="commit" type="submit" value="Sign in">
                </div>
            </form>
            <p align="center"> No account? <a href="Signup.php">Sign-up</a></p>
        </div>
    </main>
    </body>

    </html>
<?php include("Footer.php"); ?>