<?php include("header.php");?>
    <html>

    <body>
        <main>
            <!-- CONTENT -->
            <div class="col s12 grey lighten-5" id="body_content">
                <nav class="deep-purple lighten-1 reference-header">
                    <div class="nav-wrapper" id="reference_nav_wrapper">
                        <div class="left" id="sidebar_left">
                            <a class="page-title h5" id="header_page_title">Search</a>
                        </div>
                        <!--TODO: change this to point to a handler -->
                        <form accept-charset="UTF-8" action="handlers/send_message" method="get">
                            <div style="display:none">
                                <input name="utf8" type="hidden" value="✓">
                            </div>
                            <div class="input-field left col m5 l8 hide-on-small-only">
                                <div class="input-field left col m5 l8 hide-on-small-only">
                                    <input class="validate ui-autocomplete-input" id="search" minlength="3" name="search" type="text" value="" autocomplete="off">
                                    <!-- TODO: fix this to show search icon
                                    <label for="search" class=""><i class="mdi-action-search"></i></label> -->
                                    
                                </div>
                            </div>
                        </form>
                        <ul class="right hide-on-small-only">
                        </ul>
                    </div>
                </nav>
            </div>
        </main>
    </body>

    </html>
    <?php include("footer.php");?>