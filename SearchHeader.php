<?php include("Header.php"); ?>
<head>
    <!-- See Header -->
</head>

<body class="grey lighten-5">
<main>
    <div class="row" id="body_reference">

        <!-- body content -->
        <div class="col s12 grey lighten-5" id="body_content">

            <nav class="deep-purple lighten-1 reference-header">
                <div class="nav-wrapper" id="reference_nav_wrapper">
                    <div class="left" id="sidebar_left">
                        <a class="page-title h5" id="header_page_title">SHOW CURRENT SEARCH</a></div>

                    <!-- search form in header -->
                    <form accept-charset="UTF-8" action="SearchHandler.php" method="get">
                        <div style="display:none"><input name="utf8" value="✓" type="hidden"></div>

                        <div class="input-field left col m5 l8 hide-on-small-only">
                            <input class="validate ui-autocomplete-input" id="search"
                                   minlength="3" name="search" value="" autocomplete="off" type="text">
                            <label for="search" class=""></label>
                        </div>
                    </form>

                </div>
            </nav>
        </div>
    </div>
</main>
</body>